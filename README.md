# Overview

This is a template for an esp32 Dev Module (not a C or S series). The template is 
a very basic example of embassy without standard

## Running

1. Setup and install esp32 IDF environment
2. Run `. $HOME/export-esp.sh`
3. `sudo chmod a+rw /dev/tty*`
4. `cargo build`
5. `cargo espflash flash --release --monitor`