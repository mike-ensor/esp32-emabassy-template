#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use embassy_executor::Spawner;
use embassy_time::{Duration, Timer};

use esp_backtrace as _;
use esp_hal::{
    clock::ClockControl,
    embassy,
    gpio::{GpioPin, IO, Output, PushPull},
    peripherals::Peripherals,
    prelude::*,
};
use esp_println::println;
use log::{debug, error, info, warn};

type GPIOLed = GpioPin<Output<PushPull>, 2>;

#[embassy_executor::task]
async fn second_task() {
    let mut count = 0;
    loop {
        warn!("Thread: Second - {count}");
        count += 1;
        Timer::after(Duration::from_millis(1_000)).await;
    }
}

#[embassy_executor::task]
async fn blinker(mut led: GPIOLed, interval: Duration) {

    loop {
        info!("Thread: Blinker");
        led.set_high();
        debug!("Thread: Blinker - set high");
        Timer::after(interval).await;
        led.set_low();
        debug!("Thread: Blinker - set low");
        Timer::after(interval).await;
    }
}

#[main]
async fn main(spawner: Spawner) -> ! {
    error!("Init!");
    println!("Standard Print");
    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::boot_defaults(system.clock_control).freeze();
    esp_println::logger::init_logger_from_env();

    let timer_group = esp_hal::timer::TimerGroup::new_async(peripherals.TIMG0, &clocks);
    embassy::init(&clocks, timer_group);

    // Start of main loop
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
    // NOTE: GPIO 2 on esp32 is the LED (but not for every board). Using a TypeAlias read better
    let led: GPIOLed = io.pins.gpio2.into_push_pull_output();

    spawner.spawn(blinker(led, Duration::from_millis(1_000))).unwrap();
    spawner.spawn(second_task()).unwrap();

    loop {
        info!("Thread: Main");
        Timer::after(Duration::from_millis(5_000)).await;
    }
}